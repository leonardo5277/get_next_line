#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

typedef struct	s_line
{
	char		*str;
	int			newline;
}				t_line;

int				get_next_line(const int fd, char **line);

# define BUFF_SIZE 1

#endif
