#include "libft.h"

char	*ft_strpbrk(const char *s1, const char *accept)
{
	while (*s1)
		if (ft_strchr(accept, *s1++))
			return ((char*)--s1);
	return (NULL);
}
