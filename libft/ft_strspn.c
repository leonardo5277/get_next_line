#include "libft.h"

size_t		ft_strspn(const char *s1, const char *accept)
{
	size_t		value;

	value = 0;
	while (*s1 && ft_strchr(accept, *s1++))
		value++;
	return (value);
}
