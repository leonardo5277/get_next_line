char	*ft_strcpy(char *dest, const char *src)
{
	char	*s1;

	s1 = dest;
	while (*src)
		*s1++ = *src++;
	*s1 = '\0';
	return (dest);
}
