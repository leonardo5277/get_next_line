#include "libft.h"

size_t		ft_strcchar(const char *str, char c)
{
	size_t		i;

	i = 0;
	while (*str)
	{
		if (*str++ == c)
			++i;
	}
	return (i);
}
