#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list		*new_elem;
	t_list		*tmp;

	new_elem = NULL;
	while (lst)
	{
		if (new_elem == NULL)
		{
			new_elem = f(lst);
			tmp = new_elem;
		}
		else
		{
			tmp->next = f(lst);
			tmp = tmp->next;
		}
		lst = lst->next;
	}
	return (new_elem);
}
