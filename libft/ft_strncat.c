#include "libft.h"

char	*ft_strncat(char *dest, const char *src, size_t n)
{
	char		*s1;
	const char	*s2;

	s1 = dest;
	s2 = src;
	while (*s1)
		s1++;
	while ((*s2) && (n--))
		*s1++ = *s2++;
	*s1 = '\0';
	return (dest);
}
