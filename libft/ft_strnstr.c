#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t len2;

	len2 = ft_strlen(s2);
	if (!*s2)
		return ((char *)s1);
	while ((*s1) && (n-- >= len2))
	{
		if ((*s1 == *s2) && (ft_memcmp(s1, s2, len2) == 0))
			return ((char *)s1);
		s1++;
	}
	return (NULL);
}
