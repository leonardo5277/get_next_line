#include "libft.h"

void	ft_lstaddback(t_list **begin_list, t_list *new_elem)
{
	if (*begin_list == NULL)
	{
		(*begin_list) = new_elem;
		return ;
	}
	while ((*begin_list)->next)
		*begin_list = (*begin_list)->next;
	(*begin_list)->next = new_elem;
}
