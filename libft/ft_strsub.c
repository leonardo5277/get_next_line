#include "libft.h"

char	*ft_strsub(const char *str, unsigned int start, size_t len)
{
	char	*result;

	if (!str)
		return (NULL);
	result = ft_strnew(len);
	if (!result)
		return (NULL);
	str += start;
	return (ft_strncpy(result, str, len));
}
