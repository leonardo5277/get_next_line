#include "libft.h"

size_t		ft_strcspn(const char *s1, const char *reject)
{
	size_t value;

	value = 0;
	while (*s1)
	{
		if (ft_strchr(reject, *s1))
			return (value);
		else
		{
			s1++;
			value++;
		}
	}
	return (value);
}
