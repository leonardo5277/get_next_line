#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char		*s1;
	unsigned const char	*s2;

	s1 = dest;
	s2 = src;
	while (n--)
	{
		*s1 = *s2++;
		if (*s1 == (unsigned char)c)
			return ((void *)s1 + 1);
		s1++;
	}
	return (NULL);
}
